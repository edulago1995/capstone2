const express = require("express");
const productController = require("../controllers/product");
const auth = require("../auth") 
const {verify, verifyAdmin} = auth;

const router = express.Router();

router.post("/addProducts", verify, verifyAdmin, productController.addProduct);

router.get("/all", productController.getAllProducts);

router.get("/", productController.getAllActiveProducts);

router.get("/:productId",productController.getProduct);

router.post('/searchByName', productController.searchProductByName);

router.put("/:productId",verify, verifyAdmin, productController.updateProduct);

router.put("/:productId/archive", verify, verifyAdmin, productController.archiveProduct);

router.put("/:productId/activate", verify, verifyAdmin, productController.activateProduct);

router.get('/:productId/users-ordered', verify, verifyAdmin, productController.getEmailsOfUsers);

module.exports = router;