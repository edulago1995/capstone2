const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
    firstName : {
        type : String,
        required : [true, "name is required"]
    },
    lastName : {
        type : String,
        required : [true, "Last name is required"]
    },
    email : {
        type : String,
        required : [true, "Email is required"]
    },
    mobileNo : {
        type : String, 
        required : [true, "Mobile No is required"]
    },
    isPending:{
        type: Boolean,
        default: true
    },
    isActive:{
        type: Boolean,
        default: true
    },
    orderedProducts : [
       {
        products : [
            {
                productId : {
                    type : String,
                    required : [true, "Product ID is required"]
                },
                name: {
                    type : String,
                    required : [true, "Product Name is required"]
                },
                price: {
                    type : Number,
                    required : [true, "Product Price is required"]
                },
                quantity : {
                    type : Number
                }

            }
        ],
        totalAmount : {
            type : Number
        },
        purchasedOn : {
                type : Date,
                default : new Date()
        },
        }
    ]
})

module.exports = mongoose.model("Order", orderSchema);