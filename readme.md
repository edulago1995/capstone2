## E-COMMERCE API DOCUMENTATION

***TEST ACCOUNTS:***
- Regular User:
     - email: el@mail.com
     - pwd: 1234
- Admin User:
    - email: admin@mail.com
    - pwd: 1234
    

***ROUTES:***
- User registration (POST)
	- http://localhost:4000/users/register
    - request body: 
        - email (string)
        - password (string)
- User authentication (POST)
	- http://localhost:4000/users/login
    - request body: 
        - email (string)
        - password (string)
- Create Product (Admin only) (POST)
	- http://localhost:4000/products/addProducts
    - request body: 
        - name (string)
        - description (string)
        - price (number)
- Retrieve all products (Admin only) (GET)
	- http://localhost:4000/products/all
    - request body: none
- Retrieve active products (GET)
    - http://localhost:4000/products
    - request body: none
- Retrieve specific product (GET)
    - http://localhost:4000/products/64dded540f2c0a68a9d3d747
    - request body: none
- Update product (Admin only) (PUT)
    - http://localhost:4000/products/64ddec9d4778e05552eb19f9
    - request body: 
        - name (string)
        - description (string)
        - price (number)
- Archive product (Admin only) (PUT)
    - http://localhost:4000/products/64ddec9d4778e05552eb19f9/archive
- Activate product (Admin only) (PUT)
    - http://localhost:4000/products/64ddec9d4778e05552eb19f9/activate